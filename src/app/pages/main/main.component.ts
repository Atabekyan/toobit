import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'ns-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  moduleId: module.id,
})
export class MainComponent implements OnInit {

    export function showSideDrawer(args: observable.EventData) {
        console.log("Show SideDrawer tapped.");
        // Show sidedrawer ...
    }

  constructor() { }

  ngOnInit() {
  }

}
