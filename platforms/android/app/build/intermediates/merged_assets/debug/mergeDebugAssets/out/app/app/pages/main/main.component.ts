import { Component, OnInit } from '@angular/core';

import { Item } from "./item";
import { ItemService } from "./item.service";

@Component({
  selector: 'ns-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  moduleId: module.id,
})
export class MainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
